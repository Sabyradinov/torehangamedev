﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TorehanGameDev.Models;

namespace TorehanGameDev.Controllers
{
    public class HomeController : Controller
    {
        gEntities _db;

        public HomeController()
        {
            _db = new gEntities();
        }

        
        public ActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login");
            }

            int userId = Convert.ToInt32(User.Identity.Name);
            ViewBag.balance = _db.Users.Where(s => s.id == userId).FirstOrDefault().balance;
            //SendMail();
            return View();
        }

        public bool SendMail(string hashedLink, string mail)
        {
            bool res = false;
            try { 
                

                MailAddress from = new MailAddress("info@luckcounter.com", "СЧЕТЧИК УДАЧИ");
                MailAddress to = new MailAddress(mail);
                //MailAddress to = new MailAddress("sabyradinov.maksat@gmail.com");
                MailMessage m = new MailMessage(from, to);
                m.Subject = "Подтверждения почты";
                m.Body = "Для подтверждения перейдите по ссылке = " + hashedLink;
                SmtpClient smpt = new SmtpClient("luckcounter.com", 25);
                smpt.Credentials = new NetworkCredential("info@luckcounter.com", "91Klq_x1");
                smpt.EnableSsl = false;
                smpt.Send(m);
                res = true;

            }
            catch (Exception e)
            {
                res = false;
            }
            return res;
        }

        public bool SendNewPassword(string newPass, string mail)
        {
            bool res = false;
            try
            {


                MailAddress from = new MailAddress("info@luckcounter.com", "СЧЕТЧИК УДАЧИ");
                MailAddress to = new MailAddress(mail);
                //MailAddress to = new MailAddress("sabyradinov.maksat@gmail.com");
                MailMessage m = new MailMessage(from, to);
                m.Subject = "Новый пароль";
                m.Body = "Новый пароль для входа " + newPass;
                SmtpClient smpt = new SmtpClient("luckcounter.com", 25);
                smpt.Credentials = new NetworkCredential("info@luckcounter.com", "91Klq_x1");
                smpt.EnableSsl = false;
                smpt.Send(m);
                res = true;

            }
            catch (Exception e)
            {
                res = false;
            }
            return res;
        }


        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        public struct LoginResult
        {
            public int status;
            public string errorMessage;
        }

        [HttpPost]
        public JsonResult Login(LoginViewModel loginViewModel)
        {
            LoginResult result = new LoginResult();
            if (ModelState.IsValid)
            {
                var user = _db.Users.Where(s => s.email == loginViewModel.Email).FirstOrDefault();
                if (user != null)
                {
                    if (user.status == 1)
                    {
                        result.status = 2;
                        result.errorMessage = "Пользователь заблокирован!";
                    }
                    else
                    {
                        if (VerifyPassword(loginViewModel.Password, user.password, user.salt))
                        {
                            FormsAuthentication.SetAuthCookie(user.id.ToString(), true);
                            result.status = 0;
                        }
                        else
                        {
                            result.status = 1;
                            result.errorMessage = "Пароль не правильный!";
                        }
                    }
                    
                }
                else
                {
                    result.status = 1;
                    result.errorMessage = "Такой почты нету!";
                }

            }
            else
            {
                var errors = ModelState.Select(x => x.Value.Errors)
                           .Where(y => y.Count > 0)
                           .LastOrDefault();

                result.status = 1;
                result.errorMessage = errors[0].ErrorMessage;
            }

            return Json(result);
        }

        public struct InpuResult
        {
            public int status;
            public int balance;
            public string errorMessage;
        }

        [HttpPost]
        public JsonResult Testinp(int inpSumm)
        {
            InpuResult result = new InpuResult();
            int userId = Convert.ToInt32(User.Identity.Name);

            PayInsert pay = new PayInsert
            {
                insertDatetime = DateTime.Now,
                refInfo = "Test inp",
                insertSum = inpSumm,
                userId = userId,
                status = 1
            };

            _db.PayInserts.Add(pay);
            _db.SaveChanges();

            User user = _db.Users.Where(s => s.id == userId).FirstOrDefault();
            user.balance = user.balance + inpSumm;
            UpdateModel(user);
            _db.SaveChanges();

            result.status = 0;
            result.balance = Convert.ToInt32(user.balance);

            return Json(result);
        }



        public struct CaptchaResult
        {
            public int status;
            public int balance;
            public string newCapcha;
            public string errorMessage;
        }

        [HttpPost]
        public JsonResult CheckCaptcha(string capthaInp)
        {
            CaptchaResult result = new CaptchaResult();
            int userId = Convert.ToInt32(User.Identity.Name);
            var CaptchaCode = Session["CaptchaCode"].ToString();

            result.status = 1;
            if (CaptchaCode == capthaInp)
            {
                result.status = 0;

                User user = _db.Users.Where(s => s.id == userId).FirstOrDefault();
                user.balance = user.balance + 1;
                UpdateModel(user);

                CapchaInp capchaInp = new CapchaInp
                {
                    inpDatetime = DateTime.Now,
                    userId = userId
                };
                _db.CapchaInps.Add(capchaInp);
                
                _db.SaveChanges();
            }
            
            return Json(result);
        }


        public struct BetResult
        {
            public int status;
            public int summ;
            public int balance;
            public string errorMessage;
        }

        [HttpPost]
        public JsonResult MakeBet(int roomId, int betSum)
        {
            BetResult result = new BetResult();
            int userId = Convert.ToInt32(User.Identity.Name);
            var user = _db.Users.Where(s => s.id == userId).FirstOrDefault();
            var room = _db.Rooms.Where(s => s.id == roomId).FirstOrDefault();

            if (user.balance >= betSum && user.balance != 0 && room.step3 >= betSum)
            {
                try
                {
                    Bet bet = new Bet
                    {
                        userId = Convert.ToInt32(User.Identity.Name),
                        roomId = roomId,
                        betSum = betSum,
                        insertDatetime = DateTime.Now,
                        status = 1
                    };
                    _db.Bets.Add(bet);
                    _db.SaveChanges();

                    result.summ = Convert.ToInt32(getRoomSumm(roomId, bet.id));

                    if (checkWinner(result.summ, roomId))
                    {
                        WinnersHis winner = new WinnersHis
                        {
                            betId = bet.id,
                            roomId = roomId
                        };
                        _db.WinnersHis.Add(winner);
                        _db.SaveChanges();

                        user.balance = user.balance + _db.Rooms.Where(s => s.id == roomId).FirstOrDefault().summ;

                        result.balance = Convert.ToInt32(user.balance);
                        UpdateModel(user);
                        _db.SaveChanges();
                    }
                    else
                    {
                        user.balance = user.balance - betSum;

                        result.balance = Convert.ToInt32(user.balance);
                        UpdateModel(user);
                        _db.SaveChanges();
                    }

                    result.status = 0;
                }
                catch (Exception e)
                {
                    result.status = 1;
                    result.errorMessage = e.Message.ToString();
                }
            }
            else
            {
                result.status = 2;
                result.errorMessage = "Сумма баланса недостаточно";
            }
            


            return Json(result);
        }

        public bool checkWinner(int summ, int roomId)
        {
            if (summ >= _db.Rooms.Where(s => s.id == roomId).FirstOrDefault().summ) { return true; }
            else { return false; }
        }

        public double getRoomSumm(int roomId, int betId)
        {
            int lastwin = lastRoomWinner(roomId);
            var betsList = _db.Bets.Where(b => b.id > lastwin && b.roomId == roomId).ToList();
            var summ = betsList.Sum(q => q.betSum);

            return summ;
        }

        public int lastRoomWinner(int roomId)
        {
            var bet = _db.WinnersHis.Where(s => s.roomId == roomId).OrderByDescending(s => s.id);
            if (bet.Count() > 0) { return Convert.ToInt32(bet.FirstOrDefault().betId); }
            else { return 0; }
        }

        public struct RegistResult
        {
            public int status;
            public string errorMessage;
        }


        [HttpPost]
        [AllowAnonymous]
        public JsonResult Registration(RegisterViewModel RegisterViewModel)
        {
            RegistResult result = new RegistResult();
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Select(x => x.Value.Errors)
                           .Where(y => y.Count > 0)
                           .LastOrDefault();

                result.status = 1;
                result.errorMessage = errors[0].ErrorMessage;
            }
            else
            {
                var userInfo = _db.Users.Where(s => s.email == RegisterViewModel.Email.Trim()).FirstOrDefault();

                if (_db.Users.Where(s => s.email == RegisterViewModel.Email.Trim()).FirstOrDefault() != null)
                {
                    result.status = 2;
                    result.errorMessage = "Пользователь с такой почтой уже существует в системе";
                }
                else
                {
                    try {

                        PassRes resPass = HashPassword(RegisterViewModel.Password);
                        User user = new User
                        {
                            email = RegisterViewModel.Email,
                            password = resPass.hashedpass,
                            salt = resPass.salt,
                            created_date = DateTime.Now,
                            status=1
                        };

                        

                        _db.Users.Add(user);
                        _db.SaveChanges();

                        var key = Guid.NewGuid();

                        ActivationKey activationKey = new ActivationKey()
                        {
                            userId = user.id,
                            key = key.ToString()
                        };

                        _db.ActivationKeys.Add(activationKey);
                        _db.SaveChanges();

                        SendMail("http://luckcounter.com/home/confirmation?link="+ key.ToString(), user.email);
                        result.status = 0;

                    } catch (Exception e)
                    {
                        result.status = 2;
                        result.errorMessage = e.Message;
                    }
                }
            }

            return Json(result);
        }
        
        public ActionResult Confirmation(string link)
        {
            string status = "Код регистрации не правильный!";
            if (link != null)
            {
                int userId = Convert.ToInt32(_db.ActivationKeys.Where(s => s.key == link).Select(s => s.userId).FirstOrDefault());

                var user = _db.Users.Where(s => s.id == userId).FirstOrDefault();
                user.status = 0;
                UpdateModel(user);
                _db.SaveChanges();

                status = "Вы успешно прошли регистрацию!";
            }
            ViewBag.Status = status;


            return View();
        }

        


        public struct PassRes
        {
            public string hashedpass;
            public string salt;
        }

        public PassRes HashPassword(string password)
        {
            // generate a 128-bit salt using a secure PRNG
            byte[] salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }
            //Console.WriteLine($"Salt: {Convert.ToBase64String(salt)}");


            var str = Convert.ToBase64String(salt);

            // derive a 256-bit subkey (use HMACSHA1 with 10,000 iterations)
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));

            return new PassRes
            {
                hashedpass = hashed,
                salt = Convert.ToBase64String(salt)
            };
        }

        private bool VerifyPassword(string password, string hashedpassword, string bsalt)
        {
            // generate a 128-bit salt using a secure PRNG
            byte[] salt = Convert.FromBase64String(bsalt);


            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));

            if (hashed == hashedpassword) { return true; }
            return false;
        }



        public string GenerateNewPassword()
        {
            int length = 8;
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }

        public struct ResetResult
        {
            public int status;
            public string errorMessage;
        }

        [HttpPost]
        public JsonResult ResetPassword(ResetPasswordViewModel ResetPasswordViewModel)
        {
            ResetResult result = new ResetResult();

            User user = _db.Users.Where(s => s.email.ToLower() == ResetPasswordViewModel.Email.ToLower()).FirstOrDefault();

            if (user != null)
            {
                string newpassword = GenerateNewPassword();


                SendNewPassword(newpassword, user.email);
                PassRes resPass = HashPassword(newpassword);

                result.status = 0;
                user.password = resPass.hashedpass;
                user.salt = resPass.salt;
                if (ModelState.IsValid)
                {
                    UpdateModel(user);
                    _db.SaveChanges();
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors)
                               .Where(y => y.Count > 0)
                               .LastOrDefault();

                    result.status = 1;
                    result.errorMessage = errors[0].ErrorMessage;
                }

               

            }
            else
            {
                result.status = 1;
                result.errorMessage = "Пользователя с такой почтой нету!";
            }


            return Json(result);
        }

        public PartialViewResult _Login()
        {
            return PartialView("_Login", new LoginViewModel());
        }

        public PartialViewResult _Register()
        {
            return PartialView("_Register", new RegisterViewModel());
        }

        public PartialViewResult _ResetPassword()
        {
            return PartialView("_ResetPassword", new ResetPasswordViewModel());
        }


        [ChildActionOnly]
        [ActionName("_CalcBalance")]
        public ActionResult TopMenu()
        {
            int userId = Convert.ToInt32(User.Identity.Name);
            ViewBag.balance = _db.Users.Where(s => s.id == userId).FirstOrDefault().balance;
            return PartialView("_ShowBalance");
        }

        public byte[] GetCaptchaImage()
        {
            int width = 100;
            int height = 36;
            var captchaCode = Captcha.GenerateCaptchaCode();
            var result = Captcha.GenerateCaptchaImage(width, height, captchaCode);
            //HttpContext.Session.SetString("CaptchaCode", result.CaptchaCode);
            Session["CaptchaCode"] = result.CaptchaCode;
            Stream s = new MemoryStream(result.CaptchaByteData);
            //return new FileStreamResult(s, "image/png");


            return result.CaptchaByteData;
        }

        public static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        public ActionResult Mining()
        {
            var bytes = GetCaptchaImage();

            string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);

            string image = "data:image/png;base64," + base64String;

            var today = DateTime.Now.Date;
            var tomorrow = today.AddDays(1);

            ViewBag.CReamins = 800 - _db.CapchaInps.Where(s => s.inpDatetime >= today && s.inpDatetime < tomorrow).Count();
            ViewBag.Captcha = image;
            return View();
        }

        public class PayHistory
        {
            public PayHistory(int userId, gEntities _db)
            {
                PayInsert = _db.PayInserts.Where(s => s.status == 1 && s.userId == userId).ToList();
                PayOut = _db.PayOuts.Where(s => s.status == 1 && s.userId == userId).ToList();
                
            }

            public List<PayInsert> PayInsert { get; set; }
            public List<PayOut> PayOut { get; set; }
        }

        
        public ActionResult Balance()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login");
            }
            int userId = Convert.ToInt32(User.Identity.Name);

            PayHistory payHistory = new PayHistory(userId,_db);
            return View(payHistory);
        }

        public void Logout()
        {
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
        }
        
        public ActionResult Room(int roomId = 1)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login");
            }
            var model = _db.Rooms.Where(s => s.id == roomId+1).FirstOrDefault();

            ViewBag.Cnt = 0;
            if (model.summ == 10000) { ViewBag.Cnt = 1; }
            if (model.summ == 100000) { ViewBag.Cnt = 2; }
            if (model.summ == 1000000) { ViewBag.Cnt = 3; }

            return View(model);
        }

    }
}