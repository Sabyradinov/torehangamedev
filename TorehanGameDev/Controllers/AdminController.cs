﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;
//using TorehanGameDev.Models;

//namespace TorehanGameDev.Controllers
//{
//    public class AdminController : Controller
//    {
//        gEntities _db;

//        public AdminController()
//        {
//            _db = new gEntities();
//        }

//        // GET: Admin
//        public ActionResult Index()
//        {
//            var users = _db.Users.ToList();
//            return View(users);
//        }

//        public struct BlockResult
//        {
//            public int status;
//            public string errorMessage;
//        }

//        [HttpPost]
//        public JsonResult BlockUser(int userId)
//        {
//            BlockResult result = new BlockResult();

//            User user = _db.Users.Where(s => s.id == userId).FirstOrDefault();
//            user.status = 1;
//            UpdateModel(user);
//            _db.SaveChanges();

//            result.status = 0;

//            return Json(result);
//        }

//        [HttpPost]
//        public JsonResult ActivteUser(int userId)
//        {
//            BlockResult result = new BlockResult();

//            User user = _db.Users.Where(s => s.id == userId).FirstOrDefault();
//            user.status = 0;
//            UpdateModel(user);
//            _db.SaveChanges();

//            result.status = 0;

//            return Json(result);
//        }

//        public ActionResult HistoryGame()
//        {
//            var bets = _db.Bets.Where(s => s.status == 1).ToList();
//            return View(bets);
//        }

//        public ActionResult HistoryInput()
//        {
//            var payInsert = _db.PayInserts.Where(s => s.status == 1).ToList();
//            return View(payInsert);
//        }

//        public ActionResult HistoryOutput()
//        {
//            var payOut = _db.PayOuts.Where(s => s.status == 1).ToList();
//            return View(payOut);
//        }

//        public ActionResult Contacts()
//        {
//            return View();
//        }
//    }
//}