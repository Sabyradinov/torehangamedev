function blockuserbyid(userId)
{
    $.ajax({
        type: "POST",
        url: '/Admin/BlockUser',
        data: { userId: userId },
        dataType: "json",
        async: false,
        success: function (result) {
            setTimeout(function () {
                window.location.reload(1);
            }, 2000);

            //location.reload();
            //alert(result.summ);
        },
        error: function (xhr, status, error) {
            console.log("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText);
        }
    });
}

function activateuserbyid(userId) {
    $.ajax({
        type: "POST",
        url: '/Admin/ActivteUser',
        data: { userId: userId },
        dataType: "json",
        async: false,
        success: function (result) {

            setTimeout(function () {
                window.location.reload(1);
            }, 2000);
            //location.reload();
            //alert(result.summ);
        },
        error: function (xhr, status, error) {
            console.log("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText);
        }
    });
}




