$(document).ready(function () {
    $( ".datepicker" ).datepicker({
        dateFormat: "dd.mm.yy",
        monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь', 'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
        monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн', 'Июл','Авг','Сен','Окт','Ноя','Дек'],
        dayNamesMin: ['Пн','Вт','Ср','Чт','Пт','Сб','Вс']
    });

    $('.select2').select2();
    //menu add class active
    var url = window.location.href;
    $(".menu-item, .hover-y").each(function () {
        var href = $(this).attr("href");
        if (url.indexOf(href) + 1 && href !== '#') {
            $(this).addClass('active');
        }
    });

    $('.mod_close').click(function (e) {
        e.preventDefault();
        $('.modal-box').fadeOut();
        $('.modal-helper').hide();
    });

    $('.show-modal').click(function (e) {
        e.preventDefault();
        var modal_id = $(this).data('show');
        if($('.modal-box').is(':visible')){
            $('.modal-box').addClass('active').hide().promise().done(function () {
                $('#' + modal_id).show();
            });
        }else{
            $('.modal-box').addClass('active').fadeOut().promise().done(function () {
                $('#' + modal_id).fadeIn();
            });
        }
    });

    $('.show-modal2').click(function (e) {
        e.preventDefault();
        $('.modal-helper').show();
        var modal_id = $(this).data('show');
        if($('.modal-box').is(':visible')){
            $('.modal-box').addClass('active').hide().promise().done(function () {
                $('#' + modal_id).show();
            });
        }else{
            $('.modal-box').addClass('active').fadeOut().promise().done(function () {
                $('#' + modal_id).fadeIn();
            });
        }
    });
    $('.modal-helper').click(function (e) {
        e.preventDefault();
        $('.modal-helper').hide();
        $('.modal-box').addClass('active').fadeOut();
    });


    $('.room_btn').click(function () {
        var val = $(this).data('value')
        $('#room-input').val(val);
    });

    function validate(evt) {
        var theEvent = evt || window.event;
        // Handle paste
        if (theEvent.type === 'paste') {
            key = event.clipboardData.getData('text/plain');
        } else {
            // Handle key press
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
        }
        var regex = /[0-9]|\./;
        if( !regex.test(key) ) {
            theEvent.returnValue = false;
            if(theEvent.preventDefault) theEvent.preventDefault();
        }
    }
    $('.number').keypress(function () {
        validate()
    });



    function bingo() {
        $('.digit-star, .b-star, .bingo-blick, .win-text span, .back').addClass('active');
    }
    function unset_bingo() {
        $('.digit-star, .b-star, .bingo-blick, .win-text span, .back').removeClass('active');
    }

    $("#room-input").on('change', function () {
        var minSumm = $("#minSumm").val();
        var maxSumm = $("#maxSumm").val();

        if ($("#room-input").val() < minSumm )
        {
            $('#error-1').text("Сумма не может быть меньше 1 ед");
            $('#error-1').css("display", "block");

            $("#room-input").val(minSumm);
        }
        if ($("#room-input").val() > maxSumm) {

            $('#error-1').text("Сумма не может быть больше " + maxSumm);
            $('#error-1').css("display", "block");

            $("#room-input").val(maxSumm);
        }

    });

    $('button.start').click(function () {
        var val = $('#room-input').val();
        var roomId = $('#roomId').val();
        var summ = 0;

        $.ajax({
            type: "POST",
            url: '/Home/MakeBet',
            data: { roomId: roomId, betSum: val},
            dataType: "json",
            async: false,
            success: function (result) {
                if (result.status == "0")
                {
                    $("#profile_balance").each(function () {
                        $(this).text(result.balance);
                    })
                    

                    summ = result.summ;
                    var cast = summ.toString(10).split('');
                    if (roomId == 2) {
                        var j = 4;

                        for (var i = 1; j >= i; i++) {
                            $('.back-input.star_' + i).val("0");
                        }

                        for (var i = cast.length - 1; 0 <= i; i--) {
                            $('.back-input.star_' + j).val(cast[i]);
                            --j;
                        }
                    }
                    if (roomId == 3) {
                        var j = 5;
                        for (var i = 1; j >= i; i++) {
                            $('.back-input.star_' + i).val("0");
                        }

                        for (var i = cast.length - 1; 0 <= i; i--) {
                            $('.back-input.star_' + j).val(cast[i]);
                            --j;
                        }
                    }
                    if (roomId == 4) {
                        var j = 6;
                        for (var i = 1; j >= i; i++) {
                            $('.back-input.star_' + i).val("0");
                        }
                        for (var i = cast.length - 1; 0 <= i; i--) {
                            $('.back-input.star_' + j).val(cast[i]);
                            --j;
                        }
                    }
                    if (roomId == 5) {
                        var j = 7;
                        for (var i = 1; j >= i; i++) {
                            $('.back-input.star_' + i).val("0");
                        }
                        for (var i = cast.length - 1; 0 <= i; i--) {
                            $('.back-input.star_' + j).val(cast[i]);
                            --j;
                        }
                    }

                    for (var i = 0, n = cast.length; i < n; i++) {
                        console.log(cast[i]);
                    }

                    if (val < $('#minSumm').val()) {
                        $('#error-1').fadeIn();
                    } else {
                        $('#error-1').fadeOut();
                        $('button.start').attr('disabled', true);
                        if (!$('.digit-item').hasClass('active')) {
                            $('.digit-item').addClass('active');
                        } else {
                            return false
                        }

                        if (parseInt(summ, 10) >= $('#winSumm').val()) {
                            setTimeout(function () {
                                bingo();
                            }, 200);
                            setTimeout(function () {
                                unset_bingo();
                                $('button.start').attr('disabled', false);
                                $('.digit-item').removeClass('active');
                            }, 5000);
                        } else {
                            setTimeout(function () {
                                $('.digit-item').removeClass('active');
                                $('button.start').attr('disabled', false);
                            }, 2000);
                        }

                        //setTimeout(function () {
                        //    window.location.reload(1);
                        //}, 2000);
                    }

                    
                }
                else
                {
                    $('#error-1').text(result.errorMessage);
                    $('#error-1').fadeIn();
                    console.log(result.errorMessage);
                }
                

                //alert(result.summ);
            },
            error: function (xhr, status, error) {
                console.log("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText);
            }
        });
        
    });
    $('.balance-menu-item').click(function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        if($(this).hasClass('active')){
            return false;
        }
        $('.balance-item').fadeOut(200).promise().done(function () {
            $('#balance-' + id).fadeIn(200);
        });
        $('.balance-menu-item').removeClass('active');
        $(this).addClass('active');
    });
    $('.pagination-item').click(function (e) {
        e.preventDefault();
        $('.pagination-item').removeClass('active');
        $(this).addClass('active');
    });

    //$('#replenish').click(function (e) {
    //    e.preventDefault();
    //    $.post("http://185.217.180.19:8910/api/cloudpayments/pay", { publicId: "pk_c01c7369da99627ad9ca69877d49c", nameOfService: "Оплата кредита", amount: $(this).data("val"), orderId: $(this).data("id") }, function (data) {
    //        window.location.href = data.paymentURL;
    //    });
    //});

    $(".summinp").on('change', function () {
        $(".summinptng").val($(".summinp").val()*10);
    });

    $(".summinptng").on('change', function () {
        $(".summinp").val($(".summinptng").val()/10);
    });

    $('#replenishtest').click(function (e) {
        e.preventDefault();
        var inpSumm = $('.summinp').val();
        $.ajax({
            type: "POST",
            url: '/Home/Testinp',
            data: { inpSumm: inpSumm },
            dataType: "json",
            async: false,
            success: function (result) {
                $('#inpSumm1').text(inpSumm);

                $("#profile_balance").each(function () {
                    $(this).text(result.balance);
                })
                
                //location.reload();
                //alert(result.summ);
            },
            error: function (xhr, status, error) {
                console.log("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText);
            }
        });

        //e.preventDefault();
        $('.modal-box').fadeOut().promise().done(function () {
           $('#replenish-modal').fadeIn();
        });
    });

    $('#receive').click(function (e) {
        e.preventDefault();
        $('.modal-box').fadeOut().promise().done(function () {
            $('#receive-modal').fadeIn();
        });
    });

    //$('#registration_btn').click(function (e) {
    //    //e.preventDefault();
    //    $('#registration-form').fadeOut().promise().done(function () {
    //        $('#registration_success').fadeIn();
    //    });
    //});

    $('.show-form').click(function () {
        $('#registration_success').fadeOut().promise().done(function () {
            $('#registration-form').fadeIn();
        });
    });


    

    //function Timer(duration, display) {
    //    var timer = duration, hours, minutes, seconds;
    //    setInterval(function () {
    //        hours = parseInt((timer / 3600) % 24, 10)
    //        minutes = parseInt((timer / 60) % 60, 10)
    //        seconds = parseInt(timer % 60, 10);

    //        hours = hours < 10 ? "0" + hours : hours;
    //        minutes = minutes < 10 ? "0" + minutes : minutes;
    //        seconds = seconds < 10 ? "0" + seconds : seconds;

    //        display.text(hours + ":" + minutes + ":" + seconds);

    //        --timer;
    //    }, 1000);
    //}

    //jQuery(function ($) {

    //    values = $('#remainingTime').text().split(':');
    //    hours = parseInt(values[0], 10);
    //    minutes = parseInt(values[1], 10);
    //    seconds = parseInt(values[2], 10);

    //    var twentyFourHours = hours * minutes * seconds;
    //    var display = $('#remainingTime');
    //    Timer(twentyFourHours, display);
    //});


    $('#vvod').click(function (e) {
        e.preventDefault();

        var capthaInp = $('#input_captcha').val();
        $.ajax({
            type: "POST",
            url: '/Home/CheckCaptcha',
            data: { capthaInp: capthaInp },
            dataType: "json",
            async: false,
            success: function (result) {

                if (result.status == 0) {
                    $("#profile_balance").each(function () {
                        $(this).text(result.balance);
                    });

                    $('.modal-box').fadeOut().promise().done(function () {
                        $('#vvod-modal').fadeIn();
                    });

                    setTimeout(function () {
                        window.location.reload(1);
                    }, 2000);
                }
                else
                {
                    $('.error-text').css("display", "block");

                    setTimeout(function () {
                        window.location.reload(1);
                    }, 2000);
                }
                
            },
            error: function (xhr, status, error) {
                console.log("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText);
            }
        });


        
    });



    ////////////////// animations ///////////////
    if($('div').hasClass('index-logo')){
        $('.logo-items').addClass('active animated fadeIn').delay(500).queue(function () {
            $('.logo-items[data-id=2] span').addClass('active animated slideInUp').delay(50).queue(function () {
                $('.logo-items[data-id=4] span').addClass('active animated slideInUp').delay(50).queue(function () {
                    $('.logo-items[data-id=1] span, .logo-items[data-id=1] img').addClass('active animated slideInUp').delay(50).queue(function () {
                        $('.logo-items[data-id=6] span').addClass('active animated slideInUp').delay(50).queue(function () {
                            $('.logo-items[data-id=5] span').addClass('active animated slideInUp').delay(50).queue(function () {
                                $('.logo-items[data-id=7] img').addClass('active animated slideInUp').delay(50).queue(function () {
                                    $('.logo-items[data-id=3] span').addClass('active animated slideInUp').delay(500).queue(function () {
                                        $('.index-logo-top').addClass('active animated rotateIn').delay(1000).queue(function () {
                                            $('.logo-items').removeClass('animated fadeIn');
                                            $('.logo-items span, .logo-items img').removeClass('animated slideInUp');
                                            $('.index-logo-top').removeClass(' animated rotateIn')
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    }


    if($('div').hasClass('square')){
        setTimeout(function () {
            square();
        },3000);
        function square() {
                $('.center-animate2').removeClass('active');
                $('.center-animate').addClass('active');
                $('.sq8').addClass('active');
                $('.sq4').addClass('active').animate({borderWidth: '0px'}, 500, function () {
                    $('.sq7').addClass('active');
                    $('.sq3').addClass('active').animate({borderWidth: '0px'}, 500, function () {
                        $('.sq6').addClass('active');
                        $('.sq2').addClass('active').animate({borderWidth: '0px'}, 500, function () {
                            $('.sq5').addClass('active');
                            $('.sq1').addClass('active').animate({borderWidth: '0px'}, 500, function () {
                                setTimeout(function () {
                                    $('.sq5').removeClass('active');
                                    $('.sq1').removeClass('active').animate({borderWidth: '0px'}, 500, function () {
                                        $('.sq6').removeClass('active');
                                        $('.sq2').removeClass('active').animate({borderWidth: '0px'}, 500, function () {
                                            $('.sq7').removeClass('active');
                                            $('.sq3').removeClass('active').animate({borderWidth: '0px'}, 500, function () {
                                                $('.sq8').removeClass('active');
                                                $('.sq4').removeClass('active').animate({borderWidth: '0px'}, 500, function () {
                                                    setTimeout(function () {
                                                        square();
                                                    }, 5000);
                                                });
                                            });
                                        });
                                    });
                                    $('.center-animate2').addClass('active');
                                    $('.center-animate').removeClass('active');
                                }, 5000);
                            });
                        });
                    });
                });
            }
        }


    var width = $(window).width()
    if(width > 992){
        var click = 1;
        $('.info_btn').click(function (e) {
            e.preventDefault();
            if(click == 1){
                $(this).addClass('active').find('span').hide();
                $('.header-menu-animate').addClass('active');
                click = 0;
            }else{
                $('.header-menu-animate').removeClass('active');
                click = 1;
                setTimeout(function () {
                    $('.info_btn').removeClass('active').find('span').fadeIn();
                }, 200);
            }
        });
    }else{
        $('.info_btn').click(function (e) {
            e.preventDefault();
            $('.header-menu-animate').toggleClass('factive')
            $('.header-menu').toggleClass('factive').slideToggle(500);
        });

        $("body").click(function (event) {
            if ($(event.target).closest(".info_btn, .header-menu-animate, .mod_close").length === 0) {
                $('.header-menu-animate').removeClass('factive')
                $('.header-menu').removeClass('factive').slideUp(500);
            }
        });
    }


    $('.reset_pass').click(function (e) {
        //e.preventDefault();
        $('#reset').animate({opacity: 0}, 500, function () {
            $('#reset').css('visibility', 'hidden');
            $('.success_text').fadeIn();
        })

    })


});