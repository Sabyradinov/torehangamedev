﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TorehanGameDev.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class gEntities : DbContext
    {
        public gEntities()
            : base("name=gEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Bet> Bets { get; set; }
        public virtual DbSet<PayInsert> PayInserts { get; set; }
        public virtual DbSet<PayOut> PayOuts { get; set; }
        public virtual DbSet<Room> Rooms { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<WinnersHis> WinnersHis { get; set; }
        public virtual DbSet<CapchaInp> CapchaInps { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }
        public virtual DbSet<ActivationKey> ActivationKeys { get; set; }
    }
}
